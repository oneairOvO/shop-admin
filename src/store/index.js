import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
function initState(){
  return {
    token: localStorage.getItem('token')||'',
    username: localStorage.getItem('username')||''
  }
}
export default new Vuex.Store({
  state: initState(),
  mutations: {
    setToken(state,data){
      state.token=data
      localStorage.setItem("token",data)
    },
    setUsername(state,data){
      state.username=data
      localStorage.setItem("username",data)
    },
    initState(state){
      localStorage.clear()
      // state=initState()
      Object.assign(state,initState())
    }
  },
  actions: {
  },
  modules: {
  }
})
