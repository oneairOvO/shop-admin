import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
import ElementUI from "element-ui"
import 'element-ui/lib/theme-chalk/index.css'
import dayjs from "dayjs"
import MyPlugin from './plugins'
import { Dayjs } from 'dayjs'
Vue.config.productionTip = false
Vue.use(ElementUI);
// Vue.prototype.$http=http
Vue.use(MyPlugin)
Vue.filter("dealDate",val=>{
  return dayjs(val).format('YYYY-MM-DD HH:mm:ss') 
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
