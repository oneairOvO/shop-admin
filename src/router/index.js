import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login.vue'
import store from '../store/index'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/login"
  },
  {
    path: "/login",
    name: 'login',
    component: Login,
    meta: {
      notAuthorization: true
    }
  },
  {
    path: "/index",
    name: 'index',
    component: () => import('../views/Index.vue'),
    redirect: '/index/users',
    children: [
      {
        path: 'users',
        name: 'users',
        component: () => import('../views/Users.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})
//登录拦截
router.beforeEach((to,from,next)=>{
  if(!to.meta.notAuthorization){
    if(store.state.token){
      next()
    }else{
      next({
        path: '/login'
      })
    }
  }else{
    next()
  }
})

export default router
