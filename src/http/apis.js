import { http } from "./index"
import { Message } from 'element-ui'
export async function getMenus() {
    const res = await http('menus', 'get')
    return res.data
}
export async function getUsers(params) {
    const res = await http('users', 'get', {}, params)
    return res.data
}
export async function addUsers(data) {
    const res = await http("users", 'post', data)
    Message({
        type: "success",
        message: '添加成功'
    })
    return res.data
}
export async function changeState(uId, type) {
    const res = await http(`users/${uId}/state/${type}`, 'put')
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data
}
export async function editUsers(obj) {
    const res = await http(`users/${obj.id}`, "put", {
        email: obj.email,
        mobile: obj.mobile
    })
    Message({
        type: "success",
        message: res.meta.msg
    })
    return res.data
}
export async function deleteUser(id) {
    const res = await http(`users/${id}`, 'delete')
    Message({
        type: "success",
        message: res.meta.msg
    })
    return res.data
}
export async function getUsersInfo(id) {
    const res = await http(`users/${id}`)
    return res.data
}
export async function getRoles() {
    const res = await http("roles")
    return res.data
}
export async function distributeUser(id,rid){
    const res = await http(`users/${id}/role`,'put',{
        rid
    })
    Message({
        type: "success",
        message: res.meta.msg
    })
    return res.data
}