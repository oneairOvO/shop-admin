import axios from 'axios'
import { Message } from 'element-ui'
import store from "../store/index"
import router from "../router"
axios.defaults.baseURL = "https://pc.raz-kid.cn/api/private/v1/"

axios.interceptors.response.use(function (response) {
    // 在发送请求之前做些什么
    if (response.data.meta.status === '无效的token') {
        router.push({ name: 'login' })
    }
    return response;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
export const http = (url, method = 'get', data = {}, params) => {
    return new Promise((resolve, reject) => {
        axios({
            url,
            headers: {
                authorization: store.state.token
            },
            method: method.toLowerCase(),
            data,
            params
        }).then(res => {
            if (res.status >= 200 && res.status < 300 || res.status == 304) {
                if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
                    resolve(res.data)
                } else {
                    Message.error(res.data.meta.msg)
                    reject(res)
                }
            } else {
                Message.error(res.statusText)
                reject(res)
            }
        }).catch(err => {
            reject(err)
        })
    })
}